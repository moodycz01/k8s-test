#!/bin/bash

STATUS=$1

case $STATUS in

	up)
		kubectl apply -f mongo/mongodb-secret.yml
		kubectl apply -f mongo/mongodb-configmap.yml
		kubectl apply -f mongo/mongodb-depl.yml
		sleep 10
		kubectl apply -f mongo/mongo-express-depl.yml
		;;

	down)
		kubectl delete -f mongo/mongo-express-depl.yml
		kubectl delete -f mongo/mongodb-depl.yml
		kubectl delete -f mongo/mongodb-configmap.yml
		kubectl delete -f mongo/mongodb-secret.yml
		;;
esac
